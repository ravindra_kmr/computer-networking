'''
To run:
sudo python3 gitdnsserver.py 216.58.197.78 10.64.9.177 //google_ip your_ip_withinsubnetwork.

sudo python3 gitdnsserver.py 127.0.0.1 127.0.0.1 //ping yourself

'''
import socket, sys
from struct import *
import time
try:
    s = socket.socket(socket.AF_INET, socket.SOCK_RAW,socket.IPPROTO_RAW)
    recv_s = socket.socket(socket.AF_INET, socket.SOCK_RAW,1)
except:
    print("Error while socket creation.")
try:
    source_ip = sys.argv[2]
except:
    source_ip = '127.0.0.1'

dest_ip = sys.argv[1]

def check_sum(msg):
    s = 0
    for i in range(0, len(msg), 2):
        try:
            w = (msg[i]<<8) | (msg[i+1])
        except IndexError:
            w = (msg[i]<<8) | int.from_bytes(b'\x00',byteorder='big')
        s = s + w
        s = (s>>16) + (s & 0xffff);
    s = s + (s >> 16);
    s = ~s & 0xffff 
    return s
# ip header fields
def create_packet(seq=1):
    ip_ihl = 5
    ip_ver = 4
    ip_tos = 0
    ip_tot_len = 0  # kernel will fill the correct total length
    ip_id = 54321   #Id of this packet
    ip_frag_off = 0
    ip_ttl = 64
    ip_proto = 1
    ip_check = 0    # kernel will fill the correct checksum
    ip_saddr = socket.inet_aton ( source_ip )
    ip_daddr = socket.inet_aton ( dest_ip )
    '''
    above function is equivalent to this function
    def ip2int(ip_addr):
        if ip_addr == 'localhost':
            ip_addr = '127.0.0.1'
    return [int(x) for x in ip_addr.split('.')]
    '''
    ip_ihl_ver = (ip_ver << 4) + ip_ihl
    # the ! in the pack format string means network order
    ip_header = pack('!BBHHHBBH4s4s' , ip_ihl_ver, ip_tos, ip_tot_len, ip_id, ip_frag_off, ip_ttl, ip_proto, ip_check, ip_saddr, ip_daddr)
    #icmp protocol header
    ID=5050
    icmp_header =pack("!BBHHH", 8, 0, 0, ID, seq)#ID=5050
    user_data = 'Hello, i am done'
    user_data= user_data.encode('utf-8')
    psh =  icmp_header + user_data;#+psh

    icmp_check = check_sum(psh)
    icmp_header = pack("!BBHHH", 8, 0, icmp_check, ID, seq)#ID=5050

    # final full packet - syn packets dont have any data
    packet = ip_header + icmp_header + user_data
    return packet

i=0
print('PING '+str(dest_ip))
while True:
    i=i+1
    packet=create_packet(i)
    s.sendto(packet, (dest_ip , 100 ))    # put this in a loop if you want to flood the target
    start_time=time.time()
    recPacket,addr=recv_s.recvfrom(512)
    total_time= (time.time()-start_time)*1000
    print('icmp_seq={t} recieved from {x} in time {y} ms.'.format(t=i,x=dest_ip,y=total_time))
