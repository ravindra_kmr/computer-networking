'''
to run:
sudo python3 udp_pong.py 8094
nc -u 127.0.0.1 8094
'''
import socket, sys
from struct import *
import time
try:
    source_ip = sys.argv[2]
except:
    source_ip = '127.0.0.1'
s_port = int(sys.argv[1])
try:
    s = socket.socket(socket.AF_INET, socket.SOCK_RAW,socket.IPPROTO_RAW)
    recv_s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    recv_s.bind((source_ip,s_port))
except:
    print("Error while creating socket.")


def check_sum(msg):
    s = 0
    for i in range(0, len(msg), 2):
        try:
            w = (msg[i]<<8) | (msg[i+1])
        except IndexError:
            w = (msg[i]<<8) | int.from_bytes(b'\x00',byteorder='big')
        s = s + w
        s = (s>>16) + (s & 0xffff);
    s = s + (s >> 16);
    s = ~s & 0xffff 
    return s
def msg(addr):
    # ip header fields
    ip_ihl = 5
    ip_ver = 4
    ip_tos = 0
    ip_tot_len = 0  # kernel will fill the correct total length
    ip_id = 54321   #Id of this packet
    ip_frag_off = 0
    ip_ttl = 255
    ip_proto = 17
    ip_check = 0    # kernel will fill the correct checksum
    ip_saddr = socket.inet_aton ( source_ip )  
    ip_daddr = socket.inet_aton ( addr[0] )
    ip_ihl_ver = (ip_ver << 4) + ip_ihl
    # the ! in the pack format string means network order
    ip_header = pack('!BBHHHBBH4s4s' , ip_ihl_ver, ip_tos, ip_tot_len, ip_id, ip_frag_off, ip_ttl, ip_proto, ip_check, ip_saddr, ip_daddr)
    
    user_data = 'PONG\n'
    user_data= user_data.encode('utf-8')
    
    #Pseudo header
    s_ip=socket.inet_aton( source_ip)
    d_ip = socket.inet_aton(addr[0])
    zero=0
    length=8+len(user_data)
    protocol=17
    pseudo_header=pack('!4s4sBBH',s_ip,d_ip,zero,protocol,length)
    
    #UDP HEADER
    source_port=int(sys.argv[1])
    dest_port=addr[1]
    udp_header=pack('!4H',source_port,dest_port,length,0)
    
    psh =  pseudo_header+udp_header + user_data;

    udp_checksum = check_sum(psh)
    udp_header=pack('!4H',source_port,dest_port,length,udp_checksum)
    packet = ip_header + udp_header + user_data
    return(packet)

while True:
    recPacket,addr=recv_s.recvfrom(1024)
    print('Received from {x}:{y}.'.format(x=addr[0],y=addr[1]))
    packet=msg(addr)
    s.sendto(packet, addr)    
