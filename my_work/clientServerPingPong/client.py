
from socket import *
import sys
serverName = sys.argv[1]
serverPort = int(sys.argv[2])
clientSocket = socket(AF_INET, SOCK_STREAM)
clientSocket.connect((serverName,serverPort))
while True:
    try:
        sentence = input()
        clientSocket.send(sentence.encode('ascii'))
        modifiedSentence = clientSocket.recv(1024)
        print ( modifiedSentence.decode('ascii'))
        if sentence=="Good Bye":
            break;
    except:
        clientSocket.close()
clientSocket.close()

