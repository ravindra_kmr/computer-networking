
import socket
import sys
from _thread import *
import threading
    # thread fuction
def threaded(c,port):
    while True:
        try:
            # data received from client
            data = c.recv(1024)
            print("PORT:{x}-->{y}".format(x=port,y=data.decode('ascii')))
            c.send(data)
        except:
            c.close()
            s.close()
    # connection closed
    c.close()
 
 
def Main():
    host = sys.argv[1]
    port = int(sys.argv[2])
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind((host, port))
    print("socket binded to post", port)
    # put the socket into listening mode
    print("socket is listening")
    s.listen(1)
    # a forever loop until client wants to exit
    while True:
        # establish connection with client
        c, addr = s.accept()
        print('Connected to :', addr[0], ':', addr[1])
        start_new_thread(threaded, (c,addr[1],))
    s.close()

if __name__ == '__main__':
    Main()


