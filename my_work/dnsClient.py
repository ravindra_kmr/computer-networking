#to run-sudo python3 dnsClient.py www.james.bond 8.8.8.8 53
#to run - python3 dnsClient.py www.google.com 8.8.8.8 53
#to run- python3 dnsClient.py jamesbond.com 127.0.0.1 1238
import socket
import sys
import random

port = int(sys.argv[3])
siteName= sys.argv[1]
ip = sys.argv[2]
def domain():
    global siteName
    sitename=siteName+'.'
    domainparts=[]
    for i in sitename.split('.'):
        domainparts.append(i)
    return (domainparts)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

def getflags():
    rflags = ''
    QR = '0'
    OPCODE = '0000'
    AA = '0'
    TC = '0'
    RD = '0'
    RA = '0'
    Z = '000'
    RCODE = '0000'
    return int(QR+OPCODE+AA+TC+RD, 2).to_bytes(1, byteorder='big')+int(RA+Z+RCODE, 2).to_bytes(1, byteorder='big')

def printdata(data):
    print('Server:\t'+ip)
    print('Address:\t'+ip+'#'+str(port))
    print('\n')
    if int(data[3]) != 0:
        print("Error in domain name.\n")
        return
    byte=bytes(data)
    count=-1
    for i,eachbyte in enumerate(byte):
        if i == 12:
            count=i+eachbyte+1
        if(i==count and eachbyte == 0):
            break
        if i==count and eachbyte != 0:
            count=count+eachbyte+1
    count=count+4
    k=0
    for i in range (1,4):
        k=i*12 + (i-1)*4
        print('NAME:\t'+siteName)
        print('ADDRESS:\t'+str(int(byte[count+k+1]))+'.'+str(int(byte[count+k+2]))+'.'+str(int(byte[count+k+3]))+'.'+str(int(byte[count+k+4])))

def buildquestion():
    qbytes = b''
    domainname=domain()
    for part in domainname:
        length = len(part)
        qbytes += bytes([length])
        for char in part:
            qbytes += ord(char).to_bytes(1, byteorder='big')
    qbytes += (1).to_bytes(2, byteorder='big')
    qbytes += (1).to_bytes(2, byteorder='big')
    return qbytes



def buildresponse():
    TransactionID = b'\x8a\xe5'
    Flags = getflags()
    QDCOUNT = b'\x00\x01'
    ANCOUNT = (0).to_bytes(2, byteorder='big')
    NSCOUNT = (0).to_bytes(2, byteorder='big')
    ARCOUNT = (0).to_bytes(2, byteorder='big')
    dnsheader = TransactionID+Flags+QDCOUNT+ANCOUNT+NSCOUNT+ARCOUNT

    dnsquestion = buildquestion()
    return dnsheader + dnsquestion 



try:
    r = buildresponse()
    sock.sendto(r, (ip,port))
    data, addr = sock.recvfrom(512)
    printdata(data)

except:
    sock.close()
