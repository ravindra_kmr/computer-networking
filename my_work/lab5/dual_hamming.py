import itertools
import sys

k=0
r=0
n=0
def encode(s):
    """Read any no.of bits and encode it using dual hamming code. Take first k bits of input streams convert it into n bits using hamming code. Then take next k bits, convert it into n bits and so on. If there is any extra bits(not multiple of k) then it will just add it in the end."""
    z=[]
    while len(s) >= r:
        nybble = s[0:r]
        p=dual_hamming(nybble)
        if (p != None):
            z.extend(p)
        s = s[r:]
    if len(s)>0:
        z.extend(s)
    return (''.join(z))

def parity_list(bits):
    l=[]
    s=""
    for i in range (r):
        s=s+str(i)
    for j in range(1,r):
        l.extend(list(itertools.combinations(s, j+1)))
    parity_bits=[]
    for i in range(len(l)):
        t = parity(bits, l[i])
        parity_bits.append(t)
    return parity_bits

def dual_hamming(bits):
    """Return given code + parity bit between every permutation"""
    parity_bits=parity_list(bits)
    bits=list(bits)
    return(''.join(bits)+''.join(parity_bits))
    
def parity(s, indicies):
    """to find the parity at the given indicies position in s"""
    sub = ""
    for i in indicies:
        sub += s[int(i)]
    return str(str.count(sub, "1") % 2) 
def hamming_weight(a,b):
    """find the hamming weight between a and b."""
    count=0
    for i in range(len(a)):
        if not(a[i]==b[i]):
            count=count+1
    return count

def decode(bits):
    """Decode correctly with floor of (2**(r-1)-1)/2 bits of error. """
    h=[]
    h_w=[]
    for i in range(2**r):
        temp='{0:0{p}b}'.format(i,p=r)
        h.append(encode(temp))
        h_w.append(hamming_weight(bits,h[i]))
    print(h_w)
    temp=2**r
    minimum_index=0
    for i in range(len(h_w)):
        if temp>h_w[i]:
            temp=h_w[i]
            minimum_index=i
    for i in range(len(h_w)):
        if not(i==minimum_index):
            if h_w[minimum_index]==h_w[i]:
                print("Incorrect code word provided. It is nearer to two code words.")
                exit(-1)
    return (h[minimum_index][:r])


if __name__ == "__main__":
    r=int(input("enter the value of r:"))
    k=2**r-r-1
    n=2**r-1
    temp=input("Enter 1.Encoding 2.Decoding:")
    input_string = input("Enter the code(of size r for encoding and (2**r-1) for decoding):") #sys.stdin.read().strip()
    print("parity bit at the end of the code")
    if temp =='1':
        temp1=encode(input_string)
        print("Encoded string: "+str(temp1))
    elif temp == '2':
        temp1=decode(input_string)
        print("Decoded string. "+ str(temp1))

