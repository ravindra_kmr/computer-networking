
import sys

k=0
r=0
n=0
def encode(s):
    """for encoding, take first k bits of input streams convert it into n bits using hamming code. Then take next k bits, convert it into n bits and so on. If there is any extra bits(not multiple of k) then it will just add it in the end."""
    z=[]
    while len(s) >= k:
        nybble = s[0:k]
        p=hamming(nybble)
        if (p != None):
            z.extend(p)
        s = s[k:]
    if len(s)>0:
        z.extend(s)
    return (''.join(z))
def parity_list(bits):
    l=[]
    for j in range(r):
        a=[]
        for i in range(n):
            temp='{0:16b}'.format(i+1)
            l1=len(temp)
            if temp[l1-1-j]=='1':
                a.append(i)
        l.append(a)
    parity_bits=[]
    for i in range(r):
        t = parity(bits, l[i])
        parity_bits.append(t)
    return parity_bits
def hamming(bits):
    """Return given 4 bits plus parity bits for bits (1,2,3), (2,3,4) and (1,3,4)"""

    bits=list(bits)
    temp=1
    temp2=0
    for i in range(2**(r)-1):
        if(i==0 or i==temp):
            bits.insert(i,'9')
            temp2=temp2+1
            if(i!=0):
                temp=2**temp2-1
    bits=''.join(bits)
    parity_bits=parity_list(bits)
    bits=list(bits)
    temp=1
    temp2=0
    for i in range(2**(r)-1):
        if(i==0 or i==temp):
            bits[i]=parity_bits[temp2]
            temp2=temp2+1
            if(i!=0):
                temp=2**temp2-1
    return bits

def hamming_weight(a,b):
    """find the hamming weight between a and b."""
    count=0
    for i in range(len(a)):
        if not(a[i]==b[i]):
            count=count+1
    return count

def decode(bits):
    """Decode correctly with floor of (2**(r-1)-1)/2 bits of error. """
    h=[]
    h_w=[]
    for i in range(2**k):
        temp='{0:0{p}b}'.format(i,p=k)
        h.append(encode(temp))
        h_w.append(hamming_weight(bits,h[i]))
    temp=2**k
    minimum_index=0
    for i in range(len(h_w)):
        if temp>h_w[i]:
            temp=h_w[i]
            minimum_index=i
    for i in range(len(h_w)):
        if not(i==minimum_index):
            if h_w[minimum_index]==h_w[i]:
                print("Incorrect code word provided. It is nearer to two code words.")
                exit(-1)
    d_bit=[]
    temp=1
    temp2=0
    for i in range(len(h[minimum_index])):
        if(i==0 or i==temp):
            temp2=temp2+1
            if(i!=0):
                temp=2**temp2-1
        else:
            d_bit.append(h[minimum_index][i])
    return (''.join(d_bit))

              
def parity(s, indicies):
    """Compute the parity bit for the given string s and indicies"""
    sub = ""
    for i in indicies:
        sub += s[i]
    return str(str.count(sub, "1") % 2) 

########################################L###########################################
# Main

if __name__ == "__main__":
    r=int(input("enter the value of r:"))
    k=2**r-r-1
    n=2**r-1
    temp=input("Enter 1.Encoding 2.Decoding:")
    input_string = input("Enter the code (of size(2**r-r-1) for encoding and (2**r-1) for decoding):") #sys.stdin.read().strip()
    print("Parity bit at 0,1,2,4,8.. index position")
    if temp =='1':
        temp1=encode(input_string)
        print("Encoded string: "+str(temp1))
    elif temp == '2':
        temp1=decode(input_string)
        print("Decoded string. "+ str(temp1))
